// Will render a Question with multiple choices for answers.

// Options format:
// {
//   title: "Optional title for question box",
//   question: "Question text",
//   answers: [{text: "Answer text", correct: false}, ...],
//   singleAnswer: true, // or false, will change rendered output slightly.
//   singlePoint: true,  // True if question give a single point score only
//                       // if all are correct, false to give 1 point per
//                       // correct answer. (Only for singleAnswer=false)
//   randomAnswers: false  // Whether to randomize the order of answers.
// }
//
// Events provided:
// - h5pQuestionAnswered: Triggered when a question has been answered.

var H5P = H5P || {};
var base = Base.getInstance();
H5P.Lessons = function(options, contentId) {
    if (!(this instanceof H5P.Lessons))
        return new H5P.Lessons(options, contentId);
    var $ = H5P.jQuery;
    var texttemplate =
        '<div class="question-container" style="padding:1%;">' +
            '  <ul class="h5p-answers" style="width: 49%;text-align: left;float:left;">' +
            '    <% for (var i=0; i < answers.length; i++) { %>' +
            '      <li class="h5p-answer<% if (userAnswers.indexOf(i) > -1) { %> h5p-selected<% } %>">' +
            '        <label>' +
            '          <div class="h5p-input-container" style="display:none;">' +
            '            <% if (singleAnswer) { %>' +
            '            <input type="radio" name="answer" class="h5p-input" value="answer_<%= i %>"<% if (userAnswers.indexOf(i) > -1) { %> checked<% } %> />' +
            '            <% } else { %>' +
            '            <input type="checkbox" name="answer_<%= i %>" class="h5p-input" value="answer_<%= i %>"<% if (userAnswers.indexOf(i) > -1) { %> checked<% } %> />' +
            '            <% } %>' +
            '            <a width="100%" height="100%" class="h5p-radio-or-checkbox" href="#"><%= answers[i].checkboxOrRadioIcon %></a>' +
            '          </div><div class="h5p-alternative-container" data-tip="<%= answers[i].tip %>">' +
            '           <span class="h5p-span" style="padding: 1%;"><%= answers[i].title %></span>' +
            '          </div><div class="h5p-clearfix"></div>' +
            '        </label>' +
            '      </li>' +
            '    <% } %>' +
            '  </ul>' +
            '<div class="h5p-show-solution-container" style="display: none;float: left;width:47%;padding: 1%;text-align: justify;"><h1 style="margin-top:0px;"></h1><div class="description"></div></div>';
    // '<div class="h5p-show-solution-container"><div class="feedback-text"></div><a href="#" class="h5p-show-solution" style="display:none;">submit</a></div>';

    var defaults = {
        image: null,
        question: "No question text provided",
        answers: [
            {text: "Answer 1", correct: true},
        ],
        singleAnswer: false,
        singlePoint: true,
        randomAnswers: false,
        weight: 1,
        userAnswers: [],
        UI: {
            showSolutionButton: 'Show solution',
            tryAgainButton: 'Try again',
            correctText: 'Correct!',
            almostText: 'Almost!',
            wrongText: 'Wrong!'
        },
        displaySolutionsButton: true,
        postUserStatistics: (H5P.postUserStatistics === true),
        tryAgain: true,
        showSolutionsRequiresInput: true
    };
    var template = new EJS({text: texttemplate});
    var params = $.extend(true, {}, defaults, options);
    var getCheckboxOrRadioIcon = function (radio, selected) {
        var icon;
        if (radio) {
            icon = selected ? '&#xe603;' : '&#xe600;';
        }
        else {
            icon = selected ? '&#xe601;' : '&#xe602;';
        }
        return icon;
    };

    var $myDom;
    var $solutionButton;
    var $feedbackElement;
    var $feedbackDialog;
    var removeFeedbackDialog = function () {
        // Remove the open feedback dialog.
        $myDom.unbind('click', removeFeedbackDialog);
        $feedbackDialog.remove();
    };

    var score = 0;
    var solutionsVisible = false;

    var addFeedback = function ($element, feedback) {
        $('<div/>', {
            role: 'button',
            tabIndex: 1,
            class: 'h5p-feedback-button',
            title: 'View feedback',
            click: function (e) {
                if ($feedbackDialog !== undefined) {
                    if ($feedbackDialog.parent()[0] === $element[0]) {
                        // Skip if we're trying to open the same dialog twice
                        return;
                    }

                    // Remove last dialog.
                    $feedbackDialog.remove();
                }

                $feedbackDialog = $('<div class="h5p-feedback-dialog"><div class="h5p-feedback-inner"><div class="h5p-feedback-text">' + feedback + '</div></div></div>').appendTo($element);
                $myDom.click(removeFeedbackDialog);
                e.stopPropagation();
            }
        }).appendTo($element.addClass('h5p-has-feedback'));
    };

    var showSolutions = function () {
        if (solutionsVisible) {
            return;
        }

        if ($solutionButton !== undefined) {
            if (params.tryAgain) {
                $solutionButton.text(params.UI.tryAgainButton).addClass('h5p-try-again');
            }
            else {
                $solutionButton.remove();
            }
        }

        solutionsVisible = true;
        $myDom.find('.h5p-input-container').each(function (i, e) {
            var $e = $(e);
            var a = params.answers[i];
            if (a.correct) {
                $e.addClass('h5p-correct');
            }
            else {
                $e.addClass('h5p-wrong');
            }
            $e.find('input').attr('disabled', 'disabled');
//            if($e.hasClass('h5p-wrong')){
//                $e.css({
//                    background: "#c33f62"
//                });
//                $('.h5p-radio-or-checkbox').css({
//                    color: "#ffffff"
//                });
//            }
//            if($e.hasClass('h5p-correct')){
//                $e.css({
//                    background: "#39692E"
//                });
//                $('.h5p-radio-or-checkbox').css({
//                    color: "#ffffff"
//
//                });
//            }
            var c = $e.hasClass('h5p-selected');
            if (c === true && a.chosenFeedback !== undefined && a.chosenFeedback !== '') {
                addFeedback($e, a.chosenFeedback);
            }
            else if (c === false && a.notChosenFeedback !== undefined && a.notChosenFeedback !== '') {
                addFeedback($e, a.notChosenFeedback);
            }
        });
        var max = maxScore();
        if (score === max) {
            $feedbackElement.addClass('h5p-passed').html(params.UI.correctText);
        }
        else if (score === 0) {
            $feedbackElement.addClass('h5p-failed').html(params.UI.wrongText);
        }
        else {
            $feedbackElement.addClass('h5p-almost').html(params.UI.almostText);
        }
    };

    var hideSolutions = function () {
        $solutionButton.text(params.UI.showSolutionButton).removeClass('h5p-try-again');
        solutionsVisible = false;

        $feedbackElement.removeClass('h5p-passed h5p-failed h5p-almost').empty();
        $myDom.find('.h5p-correct').removeClass('h5p-correct');
        $myDom.find('.h5p-wrong').removeClass('h5p-wrong');
        $myDom.find('input').prop('disabled', false);
        $myDom.find('.h5p-feedback-button, .h5p-feedback-dialog').remove();
        $myDom.find('.h5p-has-feedback').removeClass('h5p-has-feedback');
    };

    var calculateMaxScore = function () {
        if (blankIsCorrect) {
            return params.weight;
        }
        var maxScore = 0;
        for (var i = 0; i < params.answers.length; i++) {
            var choice = params.answers[i];
            if (choice.correct) {
                maxScore += (choice.weight !== undefined ? choice.weight : 1);
            }
        }
        return maxScore;
    };

    var maxScore = function () {
        return (!params.singleAnswer && !params.singlePoint ? calculateMaxScore() : params.weight);
    };

    var added = false;
    var addSolutionButton = function () {
        if (added) {
            return;
        }
        added = true;
        $solutionButton = $myDom.find('.h5p-show-solution').show().click(function () {
            if ($solutionButton.hasClass('h5p-try-again')) {
                hideSolutions();
            }
            else {
                calcScore();
                if (answered()) {
                    showSolutions();
                    if (params.postUserStatistics === true) {
                        H5P.setFinished(contentId, score, maxScore());
                    }
                }
            }
            return false;
        });
    };

    var blankIsCorrect = true;
    for (var i = 0; i < params.answers.length; i++) {
        if (params.answers[i].correct) {
            blankIsCorrect = false;
            break;
        }
    }

    var calcScore = function () {
        score = 0;
        params.userAnswers = new Array();
        $('input', $myDom).each(function (idx, el) {
            var $el = $(el);
            if ($el.is(':checked')) {
                var choice = params.answers[idx];
                var weight = (choice.weight !== undefined ? choice.weight : 1);
                if (choice.correct) {
                    score += weight;
                }
                else {
                    score -= weight;
                }
                var num = parseInt($(el).val().split('_')[1], 10);
                params.userAnswers.push(num);
            }
        });
        if (score < 0) {
            score = 0;
        }
        if (!params.userAnswers.length && blankIsCorrect) {
            score = params.weight;
        }
        if (params.singlePoint) {
            score = (score === calculateMaxScore() ? params.weight : 0);
        }
    };
    var initLessons = function(){
//        console.debug("initLessons");
    };
    // Function for attaching the multichoice to a DOM element.
    var attach = function (target) {
        Base.setCallBack("H5P.Lessons",initLessons);
        if (typeof(target) === "string") {
            target = $("#" + target);
        } else {
            target = $(target);
        }

        // If we are reattached, forget if we have shown solutions before.
        solutionsVisible = false;

        // Recalculate icons on attach, in case we are re-attaching.
        for (var i = 0; i < params.answers.length; i++) {
            params.answers[i].checkboxOrRadioIcon = getCheckboxOrRadioIcon(params.singleAnswer, params.userAnswers.indexOf(i) > -1);
        }

        // Render own DOM into target.
        $myDom = target;
        $myDom.html(template.render(params)).addClass('h5p-lessons');

        // Add image
        if (params.image) {
            $myDom.find('.h5p-question').prepend($('<img/>', {
                src: H5P.getPath(params.image.path, contentId),
                alt: '',
                class: 'h5p-question-image'
            }));
        }

        // Create tips:
        $('.h5p-alternative-container', $myDom).each(function (){
            var $container = $(this);
            var tip = $container.data('tip');
            if(tip !== undefined && tip.trim().length > 0) {
                $container.append(H5P.JoubelUI.createTip(tip)).addClass('h5p-has-tip');
            }
        });

        $feedbackElement = $myDom.find('.h5p-show-solution-container .review-feedback');

        // Set event listeners.
        $('input', $myDom).change(function () {
            var $this = $(this);
            var num = parseInt($(this).val().split('_')[1], 10);
            if (params.singleAnswer) {
                params.userAnswers[0] = num;
                if (params.answers[num].correct) {
                    score = 1;
                } else {
                    score = 0;
                }
                $this.parents('.h5p-answers').find('.h5p-answer.h5p-selected').removeClass("h5p-selected");
                $this.parents('.h5p-answers').find('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(true, false));

                $this.parents('.h5p-answer').addClass("h5p-selected");
                $('.h5p-show-solution-container h1').html(params.answers[num].title);
                $('.h5p-show-solution-container .description').html(params.answers[num].description);
                $('.h5p-show-solution-container').css({
                    display: "block"
                });
                $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(true, true));
            } else {
                if ($this.is(':checked')) {
                    //  $this.parents('.h5p-answer').addClass("h5p-selected");
                    $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(false, true));
                } else {
                    // $this.parents('.h5p-answer').removeClass("h5p-selected");
                    $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(false, false));
                }
                calcScore();
            }
            // Triggers must be done on the returnObject.
            $(returnObject).trigger('h5pQuestionAnswered');
        });

        if (params.displaySolutionsButton === true) {
            addSolutionButton();
        }
        if (!params.singleAnswer) {
            calcScore();
        }

        $('.resource', $myDom).click(function (event) {
            openResourceUrl(getResourceFromAttribute(event.currentTarget.attributes));
            event.preventDefault();
        }).show();

        return this;
    };
    var getResourceFromAttribute = function (attributes) {
        var url = undefined;
        for(var i= 0; i < H5P.jQuery(attributes).length; i++){
            if(attributes[i].name === "resourceurl"){
                var url_temp = H5P.jQuery(attributes[i].value.split('\n'));
                for(var j=0; j < url_temp.length; j++){
                    if(url_temp[j].indexOf('http') === 0){
                        url = url_temp[j];
                    }
                }
                break;
            }
        }
        return url;
    };
    var openResourceUrl = function (resourceUrl) {
        var html = "<iframe src='"+resourceUrl +"' style=\"width:100%;height:100%;margin-top:2%;\"></iframe>";
        var pop_up = showPopup(html, $myDom);
    };

    var showPopup = function (popupContent, dom) {
        var $popup = H5P.jQuery('<div class="h5p-popup-overlay"><div class="h5p-popup-container"><div class="h5p-popup-wrapper" style="padding: 0px;">' + popupContent +
                '</div><div role="button" tabindex="1" class="h5p-button h5p-close-popup" title="Close"></div></div></div>')
            .prependTo(dom)
            .find('.h5p-close-popup')
            .click(function (event) {
                event.preventDefault();
                $popup.remove();
            })
            .end();
        return $popup;
    };

    // Initialization code
    // Randomize order, if requested
    if (params.randomAnswers) {
        params.answers = H5P.shuffleArray(params.answers);
    }
    // Start with an empty set of user answers.
    params.userAnswers = [];

    function answered() {
        return params.showSolutionsRequiresInput !== true || params.userAnswers.length || blankIsCorrect;
    };

    // Masquerade the main object to hide inner properties and functions.
    var returnObject = {
        $: $(this),
        machineName: 'H5P.Lessons',
        attach: attach, // Attach to DOM object
        getScore: function() {
            return score;
        },
        getAnswerGiven: answered,
        getMaxScore: maxScore,
        showSolutions: showSolutions,
        addSolutionButton: addSolutionButton,
        tryAgain: params.tryAgain,
        defaults: defaults // Provide defaults for inspection
    };
    // Store options.
    return returnObject;
};