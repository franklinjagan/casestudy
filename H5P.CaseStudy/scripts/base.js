/**
 * Created by sysadmin on 28/8/14.
 */
var Base = new function Base(){
    var instance = this;
    var n_event = undefined;
    var p_event = undefined;
    var slides = undefined;
    var map_call_back = {};
    var ordered_slides = [];
    var call_back_list = [];

    Base.getInstance = function(){
        return instance;
    };
    Base.staticMethod = function(){
        alert( "static method called!" );
    };
    Base.setNextEventElement = function(next_event){
        n_event = next_event;
    };
    Base.getNextEventElement = function(){
        return n_event;
    };
    Base.setPreviousEventElement = function(previous_event){
        p_event = previous_event;
    };
    Base.getPreviousEventElement = function(){
        return p_event;
    };
    Base.setSlides = function(p_slides){
        if(typeof slides === 'undefined'){
            slides = p_slides;
            for(var i = 0;i < slides.length;i++){
                for(var j = 0;j < slides[i].elements.length;j++){
                    var slide_name = slides[i].elements[j].action.library.split(" ");
                    ordered_slides.push(slide_name[0]);
                }
            }
        }
    };
    Base.setCallBack = function(type,call_back){
        for(var i = 0;i < ordered_slides.length;i++){
            if(ordered_slides[i] === type){
                call_back_list.splice(i,0,call_back);
            }
        }
    };
    Base.triggerCallBack = function(slide_number){
        for(var i = 0;i < call_back_list.length;i++){
            if(i === slide_number && typeof call_back_list[i] === 'function'){
                call_back_list[i]();
                break;
            }
        }
    };
    return Base;
};

Base.prototype.toString = function(){
    return "[object Singleton]";
};

Base.prototype.isSlideButtonActive = function(){
    return "[object Singleton]";
};

Base.prototype.addInActiveClass = function($ele){
    $ele.addClass("in-active");
};

Base.prototype.removeInActiveClass = function($ele){
    $ele.removeClass("in-active");
};

Base.prototype.hasInActiveClass = function($ele){
    return $ele.hasClass("in-active") ? true : false;
};

Base.prototype.parseHTML = function(str){
    var regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?)/ig;
    return str;
};