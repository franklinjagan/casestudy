// Will render a Question with multiple choices for answers.

// Options format:
// {
//   title: "Optional title for question box",
//   question: "Question text",
//   answers: [{text: "Answer text", correct: false}, ...],
//   singleAnswer: true, // or false, will change rendered output slightly.
//   singlePoint: true,  // True if question give a single point score only
//                       // if all are correct, false to give 1 point per
//                       // correct answer. (Only for singleAnswer=false)
//   randomAnswers: false  // Whether to randomize the order of answers.
// }
//
// Events provided:
// - h5pQuestionAnswered: Triggered when a question has been answered.

var H5P = H5P || {};
var base = Base.getInstance();
H5P.Conversation = function(options, contentId) {
    if (!(this instanceof H5P.Conversation))
        return new H5P.Conversation(options, contentId);

    var $ = H5P.jQuery;
    var texttemplate =
        '<div class="h5p-conversationquestion-top">'+
            '<div class="h5p-conversationquestion question"><div class="speech_bubble thought" style="display: none;"></div><div class="quotel-frame"><div class="quotel"></div><%= conversationQuestion %></div></div>' +
            '<div class="h5p-conversationquestion-top-right">'+
            '  <ul class="h5p-conversationanswers">' +
            '    <% for (var i=0; i < conversationAnswers.length; i++) { %>' +
            '      <li class="h5p-conversationanswer<% if (userConversationAnswers.indexOf(i) > -1) { %> h5p-selected<% } %>">' +
            '        <label>' +
            '          <div class="h5p-input-container">' +
            '            <% if (singleAnswer) { %>' +
            '            <input type="radio" name="conversationAnswer" class="h5p-input" value="conversationAnswer_<%= i %>"<% if (userConversationAnswers.indexOf(i) > -1) { %> checked<% } %> />' +
            '            <% } else { %>' +
            '            <input type="checkbox" name="conversationAnswer <%= i %>" class="h5p-input" value="conversationAnswer_<%= i %>"<% if (userConversationAnswers.indexOf(i) > -1) { %> checked<% } %> />' +
            '            <% } %>' +
            '            <a width="100%" height="100%" class="h5p-radio-or-checkbox" href="#"><%= conversationAnswers[i].checkboxOrRadioIcon %></a>' +
            '          </div><div class="h5p-alternative-container" data-tip="<%= conversationAnswers[i].tip %>">' +
            '            <span class="h5p-span"><%= conversationAnswers[i].text %></span>' +
            '          </div><div class="h5p-clearfix"></div>' +
            '        </label>' +
            '      </li>' +
            '    <% } %>' +
            '  </ul>' +
            '</div>' +
            '</div>'+
            '<div style="float: left;width: 99%;border-bottom: 1px solid #DDD;margin:0px 0.5%;">' +
            '<div class="medical-chart" style="margin:0 5px 0 5px;padding: 0px 0px;cursor: pointer;"><i class="medical-chart-icon"></i><span style="margin-left: 1%;float: left;">Medical Chart</span></div>'+
            '</div>' +
            '<div class="h5p-show-solution-container" style="display: none;">' +
            '<div style="width:50%;float:left;">'+
            '<div class="solution_feedback" ></div>' +
            '<div style="padding-top: 0px;">'+
            '<div style="padding-bottom: 40px;">'+
            '<div class="h5p-show-solution-container-heading">Expertise</div>'+
            '<div class="h5p-progressbar" style="width: 98%;overflow: hidden;left: 1%;position: relative;"><div class="h5p-completed expertise"></div></div>' +
            '</div>'+
            '<div style="padding-bottom: 15px;"><div class="h5p-show-solution-container-heading">Communication</div><div class="h5p-progressbar" style="width: 98%;overflow: hidden;left: 1%;position: relative;"><div class="h5p-completed communication" ></div></div></div>' +
            '</div>'+
            '</div>'+
            '<div class="feedback-text-frame"><div class="quotel"></div><div class="feedback-text"></div></div>'+
            '<div class="h5p-show-solution-container-image"></div></div>';
    /*var texttemplate = '<div class="layout">' +
     '<div class=""></div>'+
     '<div class="medical-chart">' +
     '<i class="medical-chart-icon"></i>' +
     '<span style="float: left;font-size: 70%;padding:8px 0 0 5px;">Medical Chart</span>'+
     '</div>'+
     '<div>' +
     '<div style="width:50%;float:left;border:1px solid red">'+
     '<div></div>'+
     '<div style="width:100%;padding:5px;border:1px solid green">'+
     '<div style="width:100%;">' +
     '<div class="h5p-show-solution-container-heading">Expertise</div>'+
     '<div class="h5p-progressbar" style="width: 98%;left: 1%;position: relative;"><div class="h5p-completed expertise"></div></div>'+
     '</div>'+
     '<div style="width:100%;">' +
     '<div class="h5p-show-solution-container-heading">Communication</div>'+
     '<div class="h5p-progressbar" style="width: 98%;left: 1%;position: relative;"><div class="h5p-completed communication"></div></div>'+
     '</div>'+
     '</div>'+
     '</div>'+
     '<div style="width:50%">' +
     '</div>'+
     '</div>'+
     '</div>';*/

    var defaults = {
        image: null,
        conversationQuestion: "No question text provided",
        conversationAnswers: [
            {text: "Answer 1", correct: true}
        ],
        singleAnswer: false,
        singlePoint: true,
        randomAnswers: false,
        weight: 1,
        userConversationAnswers: [],
        UI: {
            showSolutionButton: 'Show solution',
            tryAgainButton: 'Try again',
            correctText: 'Correct!',
            almostText: 'Almost!',
            wrongText: 'Wrong!'
        },
        displaySolutionsButton: true,
        postUserStatistics: (H5P.postUserStatistics === true),
        tryAgain: true,
        showSolutionsRequiresInput: true
    };
    var template = new EJS({text: texttemplate});
    var params = $.extend(true, {}, defaults, options);

    var getCheckboxOrRadioIcon = function (radio, selected) {
        var icon;
        if (radio) {
            icon = selected ? '&#xe603;' : '&#xe600;';
        }
        else {
            icon = selected ? '&#xe601;' : '&#xe602;';
        }
        return icon;
    };

    var $myDom;
    var $solutionButton;
    var $feedbackElement;
    var $feedbackDialog;
    var removeFeedbackDialog = function () {
        // Remove the open feedback dialog.
        $myDom.unbind('click', removeFeedbackDialog);
        $feedbackDialog.remove();
    };

    var score = 0;
    var solutionsVisible = false;

    var addFeedback = function ($element, feedback) {
        $('<div/>', {
            role: 'button',
            tabIndex: 1,
            class: 'h5p-feedback-button',
            title: 'View feedback',
            click: function (e) {
                if ($feedbackDialog !== undefined) {
                    if ($feedbackDialog.parent()[0] === $element[0]) {
                        // Skip if we're trying to open the same dialog twice
                        return;
                    }

                    // Remove last dialog.
                    $feedbackDialog.remove();
                }

                $feedbackDialog = $('<div class="h5p-feedback-dialog"><div class="h5p-feedback-inner"><div class="h5p-feedback-text">' + feedback + '</div></div></div>').appendTo($element);
                $myDom.click(removeFeedbackDialog);
                e.stopPropagation();
            }
        }).appendTo($element);
    };

    var showSolutions = function () {
        if (solutionsVisible) {
            return;
        }

        if ($solutionButton !== undefined) {
            if (params.tryAgain) {
                $solutionButton.text(params.UI.tryAgainButton).addClass('h5p-try-again');
            }
            else {
                $solutionButton.remove();
            }
        }

        solutionsVisible = true;
        $myDom.find('.h5p-conversationanswer').each(function (i, e) {
            var $e = $(e);
            var a = params.conversationAnswers[i];
            if (a.correct) {
                $e.addClass('h5p-correct');
            }
            else {
                $e.addClass('h5p-wrong');
            }
            $e.find('input').attr('disabled', 'disabled');

            var c = $e.hasClass('h5p-selected');
            if (c === true && a.chosenFeedback !== undefined && a.chosenFeedback !== '') {
                addFeedback($e, a.chosenFeedback);
            }
            else if (c === false && a.notChosenFeedback !== undefined && a.notChosenFeedback !== '') {
                addFeedback($e, a.notChosenFeedback);
            }
        });
        var max = maxScore();
        if (score === max) {
            $feedbackElement.addClass('h5p-passed').html(params.UI.correctText);
        }
        else if (score === 0) {
            $feedbackElement.addClass('h5p-failed').html(params.UI.wrongText);
        }
        else {
            $feedbackElement.addClass('h5p-almost').html(params.UI.almostText);
        }
    };

    var hideSolutions = function () {
        $solutionButton.text(params.UI.showSolutionButton).removeClass('h5p-try-again');
        solutionsVisible = false;

        $feedbackElement.removeClass('h5p-passed h5p-failed h5p-almost').empty();
        $myDom.find('.h5p-correct').removeClass('h5p-correct');
        $myDom.find('.h5p-wrong').removeClass('h5p-wrong');
        $myDom.find('input').prop('disabled', false);
        $myDom.find('.h5p-feedback-button, .h5p-feedback-dialog').remove();
    };

    var calculateMaxScore = function () {
        if (blankIsCorrect) {
            return params.weight;
        }
        var maxScore = 0;
        for (var i = 0; i < params.conversationAnswers.length; i++) {
            var choice = params.conversationAnswers[i];
            if (choice.correct) {
                maxScore += (choice.weight !== undefined ? choice.weight : 1);
            }
        }
        return maxScore;
    };

    var maxScore = function () {
        return (!params.singleAnswer && !params.singlePoint ? calculateMaxScore() : params.weight);
    };

    var addSolutionButton = function () {
        $solutionButton = $myDom.find('.h5p-show-solution').show().click(function () {
            if ($solutionButton.hasClass('h5p-try-again')) {
                hideSolutions();
                //$('.h5p-conversationanswer.h5p-selected', $myDom).removeClass('h5p-selected').find('input').attr('checked', false).end().find('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(params.singleAnswer, false));
            }
            else {
                calcScore();
                if (answered()) {
                    showSolutions();
                    if (params.postUserStatistics === true) {
                        H5P.setFinished(contentId, score, maxScore());
                    }
                }
            }
            return false;
        });
    };

    var blankIsCorrect = true;
    for (var i = 0; i < params.conversationAnswers.length; i++) {
        if (params.conversationAnswers[i].correct) {
            blankIsCorrect = false;
            break;
        }
    }

    var calcScore = function () {
        score = 0;
        params.userConversationAnswers = new Array();
        $('input', $myDom).each(function (idx, el) {
            var $el = $(el);
            if ($el.is(':checked')) {
                var choice = params.conversationAnswers[idx];
                var weight = (choice.weight !== undefined ? choice.weight : 1);
                if (choice.correct) {
                    score += weight;
                }
                else {
                    score -= weight;
                }
                var num = parseInt($(el).val().split('_')[1], 10);
                params.userConversationAnswers.push(num);
            }
        });
        if (score < 0) {
            score = 0;
        }
        if (!params.userConversationAnswers.length && blankIsCorrect) {
            score = params.weight;
        }
        if (params.singlePoint) {
            score = (score === calculateMaxScore() ? params.weight : 0);
        }
    };

    var initConversation = function(){
        (score === 0) ? base.addInActiveClass(Base.getNextEventElement()) : base.removeInActiveClass(Base.getNextEventElement());
    };

    // Function for attaching the multichoice to a DOM element.
    var attach = function (target) {
        Base.setCallBack("H5P.Conversation",initConversation);
        if (typeof(target) === "string") {
            target = $("#" + target);
        } else {
            target = $(target);
        }

        // If we are reattached, forget if we have shown solutions before.
        solutionsVisible = false;

        // Recalculate icons on attach, in case we are re-attaching.
        for (var i = 0; i < params.conversationAnswers.length; i++) {
            params.conversationAnswers[i].checkboxOrRadioIcon = getCheckboxOrRadioIcon(params.singleAnswer, params.userConversationAnswers.indexOf(i) > -1);
        }

        // Render own DOM into target.
        $myDom = target;
        $myDom.html(template.render(params)).addClass('h5p-conversation');

        // Add image
        if (params.image) {
            $myDom.find('.h5p-conversationquestion').prepend($('<img/>', {
                src: H5P.getPath(params.image.path, contentId),
                alt: '',
                class: 'h5p-conversationquestion-image'
            }));
        }

        // Create tips:
        $('.h5p-alternative-container', $myDom).each(function (){
            var $container = $(this);
            var tip = $container.data('tip');
            if(tip !== undefined && tip.trim().length > 0) {
                $container.append(H5P.JoubelUI.createTip(tip)).addClass('has-tip');
            }
        });

        $feedbackElement = $myDom.find('.h5p-show-solution-container .feedback-text');

        // Set event listeners.
        $('input', $myDom).change(function () {
            var $this = $(this);
            var num = parseInt($(this).val().split('_')[1], 10);
            if (params.singleAnswer) {
                params.userConversationAnswers[0] = num;
                if (params.conversationAnswers[num].correct) {
                    base.removeInActiveClass(Base.getNextEventElement());
                    score = 1;
                } else {
                    if(!base.hasInActiveClass(Base.getNextEventElement())){
                        base.addInActiveClass(Base.getNextEventElement());
                    }
                    score = 0;
                }
                $this.parents('.h5p-conversationanswers').find('.h5p-conversationanswer.h5p-selected').removeClass("h5p-selected");
                $this.parents('.h5p-conversationanswers').find('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(true, false));

                $this.parents('.h5p-conversationanswer').addClass("h5p-selected");
                //Arrivu-feedback
                $('.h5p-conversation .h5p-show-solution-container .feedback-text').html(params.conversationAnswers[num].text);
                $('.speech_bubble').html(params.conversationAnswers[num].speechBuuble);
                $('.h5p-conversationquestion p').html(params.conversationAnswers[num].patientSays);
                $('.solution_feedback').html(params.conversationAnswers[num].chosenFeedback);
                $('.h5p-conversation .h5p-show-solution-container').css({
                    display: "block"
                });
                $('.speech_bubble').css({
                    display: "block"
                });

                if(params.conversationAnswers[num].correct == true){
                    $('.h5p-conversation .h5p-selected > label').addClass('correct');
                }
                else{
                    $('.h5p-conversation .h5p-conversationanswer').find('.correct').removeClass('correct');
                }
                if(params.conversationAnswers[num].expertise == "poor")
                {
                    $('.expertise').css({
                        marginLeft: "25%"
                    })
                }
                else if(params.conversationAnswers[num].expertise == "fair")
                {
                    $('.expertise').css({
                        marginLeft: "50%"
                    })
                }
                else if(params.conversationAnswers[num].expertise == "good")
                {
                    $('.expertise').css({
                        marginLeft: "75%"
                    })
                }
                else if(params.conversationAnswers[num].expertise == "excellent")
                {
                    $('.expertise').css({
                        marginLeft: "95%"
                    })
                }
                else
                {
                    $('.expertise').css({
                        marginLeft: "0%"
                    })
                };

                if(params.conversationAnswers[num].communication == "poor")
                {
                    $('.communication').css({
                        marginLeft: "25%"
                    })
                }
                else if(params.conversationAnswers[num].communication == "fair")
                {
                    $('.communication').css({
                        marginLeft: "50%"
                    })
                }
                else if(params.conversationAnswers[num].communication == "good")
                {
                    $('.communication').css({
                        marginLeft: "75%"
                    })
                }
                else if(params.conversationAnswers[num].communication == "excellent")
                {
                    $('.communication').css({
                        marginLeft: "95%"
                    })
                }
                else
                {
                    $('.communication').css({
                        marginLeft: "0%"
                    })
                }
                $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(true, true));
            } else {
                if ($this.is(':checked')) {
                    $this.parents('.h5p-conversationanswer').addClass("h5p-selected");
                    $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(false, true));
                } else {
                    $this.parents('.h5p-conversationanswer').removeClass("h5p-selected");
                    $this.siblings('.h5p-radio-or-checkbox').html(getCheckboxOrRadioIcon(false, false));
                }
                calcScore();
            }
            // $('.h5p-show-solution-container').append('<div class=\"feedback-text_'+num+'\">'+ params.answers[num].chosenFeedback +'</div>');

            // Triggers must be done on the returnObject.
            $(returnObject).trigger('h5pQuestionAnswered');
        });

        if (params.displaySolutionsButton === true) {
            addSolutionButton();
        }
        if (!params.singleAnswer) {
            calcScore();
        }
        //medical-chart
        $('.medical-chart',$myDom).click(function(event){
            openMedicalChart(1,params);
            event.preventDefault();
        }).show();


        return this;
    };
    var openMedicalChart = function(page,params){
        var html = "<div id='medical-chart-parent'>"+getMedicalChart(params)+"</div>";
        var pop_up = showPopup(html,$myDom);
        //registerClickEvent($myDom,'.popup-close-button',doClose,"close",pop_up);
    };

    var getMedicalChart = function(params){
        return '<div class="medical-chart-frame">' +
            '<div style="width:100%;float:left; border-bottom: 2px solid #ededed;">' +
            '<div style="width:49%;float:left;border-right: 2px solid #ededed;">'+
            '<div style="width:100%;float:left;">'+
            '<div style="width:70%;float:left;">'+
                '<div class="patient-name-frame">'+
                    '<h1>'+params.patientName+'</h1>'+
                '</div>'+
                '<div class="patient-age-frame">'+
                    '<h3 style="float:left;margin:0">Age:</h3>'+params.patientAge +
                '</div>'+
                '<div class="patient-relationship-frame">'+
                    '<h3 style="float:left;margin:0">Relationship:</h3><p>'+params.patientRelationship+'</p>'+
                '</div>'+
            //'<div style="position: relative;width: 45%;top: 5%;"><img style="border-radius:50px;width: 30%;float:right;" src="'+H5P.getContentPath(contentId)+'/'+ params.patientImage.path +'" > </div>'+
            '</div>'+
            '<div style="width:30%;float:right;"><img style="border-radius:50px;width: 100%;" src="'+H5P.getContentPath(contentId)+'/'+ params.patientImage.path +'" ></div></div>' +
            '<div class="patient-age-frame">'+
            '<h3 style="float:left;margin:0">Menstrual History:</h3><p>Last period two weeks ago</p> ' +
            '</div>'+
            '<div class="patient-age-frame">'+
            '<h3 style="float:left;margin:0;">Previous Contraception:</h3><p>&nbsp;</p>'+
            '</div>'+
            '<div class="patient-age-frame">'+
            '<h3 style="float:left;margin:0">Smoker:</h3><p>&nbsp;</p>' +
            '</div>'+
            '<div class="patient-age-frame">'+
            '<h3 style="float:left;margin:0">Past Medical History:</h3><p>&nbsp;</p>' +
            '</div>'+
            '<div class="patient-age-frame">'+
            '<h3 style="float:left;margin:0">Family History of:</h3><p>&nbsp;</p>' +
            '<ul style="list-style: none;font-size:.8em;margin:0;"><li style="margin-bottom: -5px;">- Clots:</li><li style="margin-bottom: -5px;">- Breast/cervical cancer:</li><li style="margin-bottom: -5px;">- Migraine with aura:</li><li style="margin-bottom: -5px;">- Cardiovascular Disease:</li></ul>' +
            '</div>'+
            '</div>'+
            '<div style="width:49%;padding-left: 1%;float:left;">' +
            '<p>'+params.patientDetails+'</p>'+
            '</div>'+
            '</div>'+
            '<div style="width:100%;float:left;height:150px;">' +
            '<h3>Examination and laboratory test results:</h3>' +
            '<p></p>'+
            '</div>'+
            '<div style="width:100%;float:left;padding-bottom: 10px;border-top: 2px solid #ededed;border-bottom: 2px solid #ededed;">' +
            '<h3>Notes:</h3>' +
            '<p>'+params.notes+'</p>'+
            '</div></div>'+
            '<div><a href="javascript:void(0)" role="button" tabindex="1" class="h5p-button popup-close-button">Close</a></div>';

    };

    var goNext = function (html){
        removePage($myDom);
        html = getMedicalChartPage(getPageNumber(event.currentTarget.attributes));
        appendPage($myDom,html);
        registerClickEvent($myDom,'.popup-next-button',doClose,undefined,undefined);
    };

    var doClose = function(pop_up){
        pop_up.remove();
    };

    var registerClickEvent = function(dom,selector,call_back,type,pop_up){
        dom.find(selector).click(function(event){
            event.preventDefault();
            switch(type){
                case "close":
                    call_back(pop_up);
                    break;
                default:
                    call_back();
                    break;
            }
        });
    };

    var removePage = function(dom){
        dom.find('#medical-chart-parent').empty();
    };

    var appendPage = function(dom,html){
        dom.find('#medical-chart-parent').append(html);
    };

    var getPageNumber = function(attr){
        var value = undefined;
        for(var i =0;i < attr.length;i++){
            if(H5P.jQuery(attr[i])[0].name === "page"){
                value = H5P.jQuery(attr[i])[0].value;
                break;
            }
        }
        return value;
    };

    var getMedicalChartPage = function(page){
        var html = undefined;
        switch(parseInt(page)){
            case 2:
                var case1 = "Kijai is part of a community where women are somewhat free to " +
                    "make their own medical decisions without the intervention of their " +
                    "husbands or family members. Women in her culture are mostly independent. " +
                    "They are respected members of the communities they live in, and many women continue to work outside the home after they have children. " +
                    "Women from older generations still hold some misconceptions about contraceptive methods that they pass on to the younger generations.";

                var case2 = "Any additional personal and cultural information you learn about " +
                    "Kijai as you continue through this conversation will appear here.";

                html = '<div class="h5p-copyinfo-header">Page3</div>' +
                    '<div><p>'+case1+'</p></div>'+
                    '<div><p>'+case2+'</p></div>'+
                    '<div><a href="javascript:void(0)" class="popup-next-button">Close</a></div>';
                break;
            default:
                var paragraph1 = "Kijai has already filled out some basic information " +
                    "about herself while she was waiting for the consultation to begin, " +
                    "but you’ll notice that there are still some blank fileds in her medical chart. ";

                var paragraph2 = "In working with her today, part of your goal will be to fill in those blanks. " +
                    "As you move through your discussion with Kijai and gain more information about her, " +
                    "this chart will automatically update with the information you have learned.";

                html = '<div class="h5p-copyinfo-header">Medical Chart</div>' +
                    '<div><p>'+paragraph1+'</p></div>'+
                    '<div><p>'+paragraph2+'</p></div>'+
                    '<div><a href="javascript:void(0)" class="popup-next-button">Close</a></div>';
                break;
        }
        return html;
    };

    var showPopup = function(popupContent,dom){
        var $popup = H5P.jQuery('<div class="h5p-popup-overlay"><div class="h5p-popup-container"><div class="h5p-popup-wrapper">' + popupContent +
                '</div><div role="button" tabindex="1" class="h5p-button h5p-close-popup" title="Close" style="display: none;"></div></div></div>')
            .prependTo(dom)
            .find('.popup-close-button')
            .click(function(event) {
                event.preventDefault();
                $popup.remove();
            })
            .end();
        return $popup;
    };
    // Initialization code
    // Randomize order, if requested
    if (params.randomAnswers) {
        params.conversationAnswers = H5P.shuffleArray(params.conversationAnswers);
    }
    // Start with an empty set of user answers.
    params.userConversationAnswers = [];

    function answered() {
        return params.showSolutionsRequiresInput !== true || params.userConversationAnswers.length || blankIsCorrect;
    };
    // Masquerade the main object to hide inner properties and functions.
    var returnObject = {
        $: $(this),
        machineName: 'H5P.Conversation',
        attach: attach, // Attach to DOM object
        getScore: function() {
            return score;
        },
        getAnswerGiven: answered,
        getMaxScore: maxScore,
        showSolutions: showSolutions,
        addSolutionButton: addSolutionButton,
        tryAgain: params.tryAgain,
        defaults: defaults // Provide defaults for inspection
    };
    // Store options.
    return returnObject;

};